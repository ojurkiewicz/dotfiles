eval "$(starship init zsh)"

alias mysz="bluetoothctl power off && bluetoothctl power on"
alias cat="bat"
alias ls="exa"

# Set up Node Version Manager
source /usr/share/nvm/init-nvm.sh
